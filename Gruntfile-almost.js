'use strict';
var LIVERELOAD_PORT = 35729;
var SERVER_PORT = 9000;
var lrSnippet = require('connect-livereload')({port: LIVERELOAD_PORT});
var mountFolder = function (connect, dir) {
    return connect.static(require('path').resolve(dir));
};

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to match all subfolders:
// 'test/spec/**/*.js'
// templateFramework: 'lodash'

module.exports = function (grunt) {
    // show elapsed time at the end
    require('time-grunt')(grunt);
    // load all grunt tasks
    require('load-grunt-tasks')(grunt);

    // configurable paths
    //var yeomanConfig = {
        //app: 'app',
        //dist: 'dist'
    //};

    grunt.initConfig({
        watch: {
            options: {
                nospawn: true
            },
            //compass: {
                //files: ['app/styles/{,*/}*.{scss,sass}'],
                //tasks: ['compass']
            //},
            //jst: {
                //files: [
                    //'<%= yeoman.app %>/scripts/templates/*.ejs'
                //],
                //tasks: ['jst']
            //},
            //test: {
                //files: ['<%= yeoman.app %>/scripts/{,*/}*.js', 'test/spec/**/*.js'],
                //tasks: ['test:true']
            //}
        },
        connect: {
            options: {
                port: grunt.option('port') || SERVER_PORT,
                // change this to '0.0.0.0' to access the server from outside
                hostname: 'localhost'
            },
            dist: {
                options: {
                    middleware: function (connect) {
                        return [
                            mountFolder(connect, yeomanConfig.dist)
                        ];
                    }
                }
            }
        },
        clean: {
            dist: ['.tmp', 'app/dist'],
            server: '.tmp'
        },
        //jshint: {
            //options: {
                //jshintrc: '.jshintrc',
                //reporter: require('jshint-stylish')
            //},
            //all: [
                //'Gruntfile.js',
                //'<%= yeoman.app %>/scripts/{,*/}*.js',
                //'!<%= yeoman.app %>/scripts/vendor/*',
                //'test/spec/{,*/}*.js'
            //]
        //},
        //mocha: {
            //all: {
                //options: {
                    //run: true,
                    //urls: ['http://localhost:<%= connect.test.options.port %>/index.html']
                //}
            //}
        //},
        compass: {
            options: {
                sassDir: 'scss',
                cssDir: '.tmp/css',
                //imagesDir: '<%= yeoman.app %>/images',
                javascriptsDir: 'app/js',
                //fontsDir: '<%= yeoman.app %>/styles/fonts',
                //importPath: '<%= yeoman.app %>/bower_components',
                importPath: 'app/bower_components',
                relativeAssets: true
            },
            dist: {},
            server: {
                options: {
                    debugInfo: true
                }
            }
        },
        //concat: {
          //dist: {}
        //},
        uglify: {
            dist: {
                files: {
                    'app/dist/js/main.js': [
                        'app/dist/js/main.js'
                    ]
                }
            }
        },
        requirejs: {
          dist: {
              // Options: https://github.com/jrburke/r.js/blob/master/build/example.build.js
              options: {
                  baseUrl: 'app/js',
                  optimize: 'none',
                  //paths: {
                      //'templates': '../../.tmp/js/templates'
                  //},
                  preserveLicenseComments: false,
                  useStrict: true,
                  name: 'main',
                  out: 'app/dist/js/main.js',
                  mainConfigFile: 'app/js/main.js'
              }
          }
      },
        useminPrepare: {
            html: 'index.html',
            options: {
                dest: '/app/dist'
            }
        },
        usemin: {
            html: ['dist/{,*/}*.html'],
            //css: ['dist/css/{,*/}*.css'],
            css: ['app/dist/css/styles.css'],
                options: {
                  //dirs: ['dist']
                  assetsDirs: ['dist/css', 'dist/js']
                }
        },
        cssmin: {
            dist: {
                files: {
                    'app/dist/css/styles.css': [
                        '.tmp/css/{,*/}*.css',
                        'app/css/{,*/}*.css'
                    ]
                }
            }
        },
        htmlmin: {
            dist: {
                options: {
                    /*removeCommentsFromCDATA: true,
                    // https://github.com/yeoman/grunt-usemin/issues/44
                    //collapseWhitespace: true,
                    collapseBooleanAttributes: true,
                    removeAttributeQuotes: true,
                    removeRedundantAttributes: true,
                    useShortDoctype: true,
                    removeEmptyAttributes: true,
                    removeOptionalTags: true*/
                },
                files: [{
                    expand: true,
                    cwd: 'app',
                    src: '*.html',
                    dest: 'app/dist'
                }]
            }
        },
        copy: {
          main: {
            files: [{
            src: 'app/bower_components/foundation-icon-fonts/foundation-icons.{eot,svg,ttf,woff}',
            dest: 'app/dist/css',
            expand: true,
            flatten: true
          },
          {
            src: 'app/bower_components/requirejs/require.js',
            dest: 'app/dist/js',
            expand: true,
            flatten: true
          }
          ]
          }
        },
        bower: {
            all: {
                rjsConfig: '<%= yeoman.app %>/scripts/main.js'
            }
        },
        jst: {
            options: {
                amd: true
            },
            compile: {
                files: {
                    '.tmp/js/templates.js': ['app/js/templates/*.html']
                }
            }
        },
        rev: {
            dist: {
                files: {
                    src: [
                        'dist/js/{,*/}*.js',
                        'dist/css/{,*/}*.css'
                        //'<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp}',
                        //'/styles/fonts/{,*/}*.*',
                        //'bower_components/sass-bootstrap/fonts/*.*'
                    ]
                }
            }
        }
    });

    grunt.registerTask('createDefaultTemplate', function () {
        grunt.file.write('.tmp/scripts/templates.js', 'this.JST = this.JST || {};');
    });

    grunt.registerTask('serve', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'open:server', 'connect:dist:keepalive']);
        }

        if (target === 'test') {
            return grunt.task.run([
                'clean:server',
                'createDefaultTemplate',
                'jst',
                'compass:server',
                'connect:test',
                'open:test',
                'watch'
            ]);
        }

        grunt.task.run([
            'clean:server',
            'createDefaultTemplate',
            'jst',
            'compass:server',
            'connect:livereload',
            'open:server',
            'watch'
        ]);
    });

    grunt.registerTask('build', [
        'clean:dist',
        //'createDefaultTemplate',
        //'jst',
        'compass:dist',
        'useminPrepare',
        'requirejs',
        //'imagemin',
        'htmlmin',
        //'concat',
        'cssmin',
        'uglify',
        'copy:main',
        'rev',
        'usemin'
    ]);

    grunt.registerTask('default', [
        'jshint',
        'test',
        'build'
    ]);
};


