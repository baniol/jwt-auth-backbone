module.exports = function (grunt) {
    // show elapsed time at the end
    require('time-grunt')(grunt);
    // load all grunt tasks
    require('load-grunt-tasks')(grunt);

    // configurable paths
    var yeomanConfig = {
        app: 'app',
        scss: 'scss',
        fonts: 'bower_components/foundation-icon-fonts'
    };

    grunt.initConfig({
        yeoman: yeomanConfig,
        watch: {
            options: {
                nospawn: false,
                //livereload: true
            },
            compass: {
                files: ['<%= yeoman.scss %>/_main.scss'],
                tasks: ['sass']
            },
            test: {
                files: ['<%= yeoman.app %>/scripts/{,*/}*.js', 'test/spec/**/*.js'],
                tasks: ['test:true']
            }
        },
        copy: {
          main: {
            src: '<%= yeoman.fonts %>/foundation-icons.{eot,svg,ttf,woff}',
            dest: '<%= yeoman.app %>/css/',
            expand: true,
            flatten: true
          },
        },
        connect: {
          server: {
              options: {
                port: SERVER_PORT,
                base: 'app'
              }
          }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                'Gruntfile.js',
                '<%= yeoman.app %>/scripts/{,*/}*.js',
                '!<%= yeoman.app %>/scripts/vendor/*',
                'test/spec/{,*/}*.js'
            ]
        },
        sass: {
          dev: {
            options: {
              //style: 'compressed'
            },
            files: {
              '<%= yeoman.app %>/css/styles.css':'<%= yeoman.scss %>/styles.scss'
            }
          }
        },
        requirejs: {
            dist: {
                // Options: https://github.com/jrburke/r.js/blob/master/build/example.build.js
                options: {
                    baseUrl: '<%= yeoman.app %>/scripts',
                    optimize: 'none',
                    paths: {
                        'templates': '../../.tmp/scripts/templates',
                        'jquery': '../../<%= yeoman.app %>/bower_components/jquery/dist/jquery',
                        'underscore': '../../<%= yeoman.app %>/bower_components/lodash/dist/lodash',
                        'backbone': '../../<%= yeoman.app %>/bower_components/backbone/backbone'
                    },
                    // TODO: Figure out how to make sourcemaps work with grunt-usemin
                    // https://github.com/yeoman/grunt-usemin/issues/30
                    //generateSourceMaps: true,
                    // required to support SourceMaps
                    // http://requirejs.org/docs/errors.html#sourcemapcomments
                    preserveLicenseComments: false,
                    useStrict: true,
                    wrap: true
                    //uglify2: {} // https://github.com/mishoo/UglifyJS2
                }
            },
            test: {
              options: {
                baseUrl: 'app/js',
                //paths: {
                    //'templates': '../../.tmp/scripts/templates',
                    //'jquery': '../../<%= yeoman.app %>/bower_components/jquery/dist/jquery',
                    //'underscore': '../../<%= yeoman.app %>/bower_components/lodash/dist/lodash',
                    //'backbone': '../../<%= yeoman.app %>/bower_components/backbone/backbone'
                //},
                //wrap: true,
                // name: "../libs/almond",
                //onModuleBundleComplete: amdcleanLogic,
                //preserveLicenseComments: false,
                //optimize: 'uglify',
                //optimizeCss: 'standard',
                name: 'main',
                mainConfigFile: 'app/js/config-require.js',
                //include: ['mobile'],
                out: 'dist/app.min.js'
              }
            }
        },
    });

    grunt.registerTask('serve', function (target) {
        grunt.task.run([
            'copy:main',
            'sass:dev',
            'connect',
            'watch'
        ]);
    });

    grunt.registerTask('build', [
        //'clean:dist',
        //'createDefaultTemplate',
        //'jst',
        //'compass:dist',
        //'useminPrepare',
        'requirejs',
        //'imagemin',
        //'htmlmin',
        //'concat',
        //'cssmin',
        //'uglify',
        //'copy',
        //'rev',
        //'usemin'
    ]);
};
