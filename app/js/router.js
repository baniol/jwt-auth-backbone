define(function(require) {
  'use strict';

  var Backbone = require('backbone');
  var IndexView = require('views/index');
  var LoginView = require('views/auth/login');
  var RegisterView = require('views/auth/register');
  var HeaderView = require('views/header');
  var AccountView = require('views/auth/account');
  var ForgotView = require('views/auth/forgot');
  var utils = require('utils');
  var config = require('config');

  var Router = Backbone.Router.extend({

    initialize: function () {
      var self = this;

      Backbone.history.start();

      Backbone.history.on("all", function (route, router) {
        utils.clearAlert();
        var hash = window.location.hash;
      });

      var headerView = new HeaderView();
      $('.header').html(headerView.el); // @TODO more context

      this.checkAuth();
    },

    before: {
      '' : 'checkAuth',
      'account' : 'checkAuth'
    },

    routes: {
      ''                : 'index',
      'login'           : 'login',
      'logout'          : 'logout',
      'register'        : 'register',
      'account'         : 'editAccount',
      'forgot-password' : 'forgotPassword',
      '*path'           : 'notFound'
    },

    // https://paydirtapp.com/blog/backbone-in-practice-memory-management-and-event-bindings/
    // @TODO detailed read - teardown ??
    render: function () {
      this._currentView = this.view; // ? see link above
      // @TODO to config?
      $('#main').html(this.view.render().el);
    },

    index: function () {
      this.view = new IndexView();
      this.render();
    },

    // # Auth routes
    register: function () {
      this.view = new RegisterView();
      this.render();
    },

    editAccount: function () {
      this.view = new AccountView();
      this.render();
    },

    login: function () {
      this.view = new LoginView();
      this.render();
    },

    logout: function() {
      var self = this;
      $.ajax({
        url: config.domain + '/logout',
        // @TODO
        headers: {
          token: utils.getUser().token
        },
        type: 'POST'
      })
        .always(function () {
          // Remove localStorage user data
          // @TODO separate method - the same in remove account
          localStorage.removeItem(config.lsAuth);
          Backbone.trigger('auth:unauthorized');
        });
    },

    forgotPassword: function () {
      this.view = new ForgotView();
      this.render();
    },

    // @TODO separate module ?
    // @TODO - arguments ?
    checkAuth: function (fragment, args, next) {
      var token = utils.getUser().token;
      // Fix for caching headers
      // @TODO revise
      $.ajaxSetup({
        beforeSend: function(jqXHR, settings) {
          jqXHR.setRequestHeader('Authorization', 'Bearer ' + token);
        }
      })
      var self = this;
      $.ajax({
        url: config.domain + '/checkauth',
        type: 'POST'
        //contentType:'application/json'
      })
      .done(function(res) {
        Backbone.trigger('auth:authorized');
        if (next) {
          next();
        }
      })
      .fail(function(res) {
        Backbone.trigger('auth:unauthorized');
        if (next) {
          next();
        }
      });
    },

    notFound: function () {
      // @TODO finish
      console.log('not found');
    }

  });

  return Router;

});
