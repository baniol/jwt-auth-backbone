require.config({
  shim: {
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: [
        'underscore',
        'jquery'
      ],
      exports: 'Backbone'
    },
    foundation: {
      deps: [
        'jquery'
      ],
      exports: 'foundation'
    }
  },
  paths: {
    jquery: '../bower_components/jquery/dist/jquery.min',
    underscore: '../bower_components/underscore/underscore-min',
    backbone: '../bower_components/backbone/backbone',
    backboneRouteFilter: '../bower_components/backbone-route-filter/backbone-route-filter-min',
    foundation: '../bower_components/foundation/js/foundation.min',
    text: '../bower_components/requirejs-text/text',
  }
});

require([
  'backbone',
  'router',
  'backboneRouteFilter'
], function(Backbone, Router) {

  var backboneSync = Backbone.sync;
  var utils = require('utils');

  Backbone.sync = function (method, model, options) {
    var token = utils.getUser().token;
    options.error = function (err) {
      if (err.statusText === 'Unauthorized') {
        Backbone.trigger('auth:unauthorized');
      }
    };
    // @TODO what is it for - header is set in router.js
    options.headers = {
      'Authorization': 'Bearer ' + token
    };
    backboneSync(method, model, options);
  };

  new Router();
});
