define(function (require) {

  var config = require('config');

  var utils = {};

  utils.validate = function ($el, error) {
    error = error.map(function (el) {
      return {field: el.param, msg: el.msg};
    });
    $el.find('.columns.error').removeClass('error');
    var inputs = $el.find('[data-valid]');
    _.each(inputs, function (el) {
      var name = $(el).data('valid');
      _.each(error, function (er) {
        if (name === er.field) {
          var $column = $(el).closest('.columns');
          $column.addClass('error');
          $column.find('small.error').text(er.msg);
        }
      });
    });
  };

  utils.alertFlash = {};

  utils.alert = function (type, msg, flash) {
    var $alertBox = $('.alert-box');
    $alertBox.addClass(type).text(msg).show();
    if (flash) {
      utils.alertFlash.type = type;
      utils.alertFlash.msg = msg;
    }
    else {
      setTimeout(function () {
        $alertBox.fadeOut('medium', function () {
          $alertBox.removeClass('success alert info');
        });
      }, 3000);
    }
  };

  utils.clearAlert = function () {
    // @TODO duplication
    var $alertBox = $('.alert-box');
    if (utils.alertFlash.msg) {
      $alertBox.addClass(utils.alertFlash.type).text(utils.alertFlash.msg).show();
      utils.alertFlash = {};
      setTimeout(function () {
        $alertBox.fadeOut().removeClass('success alert');
      }, 3000);
    }
    else {
      $alertBox.removeClass('success alert info').hide();
    }
  };

  utils.ajaxSetup = function (token) {
    $.ajaxSetup({
      beforeSend: function(jqXHR, settings) {
        jqXHR.setRequestHeader('token', token);
      }
    })
  };

  utils.getUser = function () {
    return localStorage.getItem(config.lsAuth) ? JSON.parse(localStorage.getItem(config.lsAuth)) : {};
  };

  utils.setUser = function (token) {
    //var old = utils.getUser(); // @TODO - not use in this scope ??
    var payload = JSON.parse(window.atob(token.split('.')[1]));
    var user = payload.user;
    console.log(user);
    var newUser = {username: user.name ,email: user.email, token: token};
    localStorage.setItem(config.lsAuth, JSON.stringify(newUser));
    // @TODO probably the reason for 'token caching' in router's checkAuth method !!!
    utils.ajaxSetup(token);
  };

  utils.refreshToken = function (newToken) {
    var user = utils.getUser();
    var refreshedUser = {username: user.username ,email: user.email, token: newToken};
    localStorage.setItem(config.lsAuth, JSON.stringify(refreshedUser));
    // @TODO probably the reason for 'token caching' in router's checkAuth method !!!
    utils.ajaxSetup(newToken);
  };

  utils.loader = function ($el, text) {
    text = text || '';
    var $button = $el.find('[type=submit]');
    utils.btnCache = $button.text();
    $button.addClass('sending');
    $button.html('<span>' + text + '</span><i class="fi-refresh"></i>');
    $button.attr('disabled', true);
  };

  utils.removeLoader = function ($el) {
    // @TODO button to utils global ?
    var $button = $el.find('[type=submit]');
    $button.removeClass('sending');
    $button.text(utils.btnCache);
    $button.removeAttr('disabled');
  };

  // @TODO to auth module
  utils.keepAlive = function () {
    if (!utils._keepAlive) {
      utils._keepAlive = setInterval(function () {
        $.ajax({
          url: config.domain + '/keepalive',
          // headers: {token: utils.getUser().token},
          type: 'GET'
        })
        .done(function (res) {
          utils.refreshToken(res.token);
        })
        .fail(function (res) {
          Backbone.trigger('auth:unauthorized');
        });
      }, config.keepAliveInterval);
    }
  };

  utils.resetKeepAlive = function () {
    clearInterval(utils._keepAlive);
    utils._keepAlive = undefined;
  };

  return utils;
});
