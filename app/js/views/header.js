define(function(require) {
  'use strict';

  var Backbone = require('backbone');
  var HeaderTemplate = require('text!templates/header.html');
  var foundation = require('foundation');
  var utils = require('utils');
  var config = require('config');

  var HeaderView = Backbone.View.extend({

    logged: false,

    initialize: function () {
      var self = this;
      this.render();
      this.$header = this.$el.find('.top-bar');
      Backbone.on('auth:unauthorized', function () {
        utils.resetKeepAlive();
        // @TODO - try token revoke here? (rather on server-side) (separate method, maybe module auth)
        // self.$header.removeClass('authorized').addClass('unauthorized');
        self.logged = false;
        self.render();
        // Backbone.history.navigate('', {trigger: true});
      });
      Backbone.on('auth:authorized', function (res) {
        // clearInterval(utils.keepAlive);
        utils.keepAlive();
        // Only on userdata change
        //if (user && user.token) {
        if (res) {
          utils.setUser(res);
        }
        // self.$header.removeClass('unauthorized').addClass('authorized');
        self.logged = true;
        self.render();
      });
    },

    template: _.template(HeaderTemplate),

    render: function () {
      $(this.el).html(this.template({logged: this.logged, username: utils.getUser().username}));
      // @TODO - to initialize ?
      $(this.el).foundation({
        topbar: {
          custom_back_text: false,
          is_hover: true,
          mobile_show_parent_link: true
        }
      });
      return this;
    }

  });

  return HeaderView;

});
