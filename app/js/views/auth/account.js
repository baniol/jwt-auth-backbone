define(function (require) {
  'use strict';

  var Backbone = require('backbone');
  var IndexTemplate = require('text!templates/auth/account.html');
  var utils = require('utils');
  var config = require('config');

  var AccountView = Backbone.View.extend({

    initialize: function () {
      this.render();
    },

    events: {
      "submit .account-form"  : "update",
      "click .remove-account" : "removeAccount"
    },

    template: _.template(IndexTemplate),

    render: function () {
      this.$el.html(this.template({user: utils.getUser()}));
      return this;
    },

    update: function (e) {
      e.preventDefault();
      var self = this;
      this.$el.find('.columns.error').removeClass('error');
      utils.loader(this.$el, 'Sending');
      var credentials = {
        username: this.$('.username-input').val(),
        email: this.$('.email-input').val(),
        password: this.$('.password-input').val(),
        confirmPassword: this.$('.password-repeat-input').val()
      };
      $.ajax({
        url: config.domain + '/editaccount',
        data: credentials,
        type: 'POST',
        headers: {
          token: utils.getUser().token
        }
      })
      .done(function (res) {
        if (res && res.msg === 'data_not_modified') {
          utils.alert('info', 'User data not modified');
          return;
        }
        // To change user details in top bar
        Backbone.trigger('auth:authorized', res);
        utils.alert('success', 'Account data changed');
        $('.password-input').val('');
        $('.password-repeat-input').val('');
      })
      .fail(function  (res, status, message) {
        // @TODO more universal for expired handling
        if (status === 'error' && message === 'Unauthorized') {
          Backbone.trigger('auth:unauthorized');
          return;
        }
        utils.validate(self.$el.find('form'), res.responseJSON);
      })
      .always(function () {
        utils.removeLoader(self.$el);
      });
    },

    removeAccount: function (e) {
      e.preventDefault();
      if (confirm ('Are you ok with removing your acount?')) {
        $.ajax({
          url: config.domain + '/removeaccount',
          type: 'POST',
          headers: {
            token: utils.getUser().token
          }
        })
        .done(function (res) {
          // Remove localStorage user data
          // @TODO separate method - the same in logout
          localStorage.removeItem(config.lsAuth);
          Backbone.trigger('auth:unauthorized');
        })
        .fail(function  (res, status, message) {
          // @TODO log errors
          console.log('error');
        });
      }
    }
  });

  return AccountView;
});
