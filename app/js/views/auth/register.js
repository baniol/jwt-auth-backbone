define(function (require) {
  'use strict';

  var Backbone = require('backbone');
  var LoginTemplate = require('text!templates/auth/register.html');
  var utils = require('utils');
  var config = require('config');

  var RegisterView = Backbone.View.extend({

    initialize: function () {
      this.render();
    },

    events: {
      "submit .register-form": "register"
    },

    template: _.template(LoginTemplate),

    render: function () {
      this.$el.html(this.template());
      return this;
    },

    register: function (e) {
      var self = this;
      e.preventDefault();
      utils.loader(this.$el, 'Registering');
      var credentials = {
        username: this.$('.username-input').val().trim(),
        email: this.$('.email-input').val().trim(),
        password: this.$('.password-input').val().trim()
      };
      $.ajax({
        url: config.domain + '/signup',
        data: JSON.stringify(credentials),
        contentType:'application/json',
        type: 'POST'
      })
      .done(function(res) {
        if (res) {
          Backbone.trigger('auth:authorized', res);
          // @TODO check
          utils.alert('success', 'You are now registered', true);
          Backbone.history.navigate('', {trigger: true});
        }
        // localStorage.setItem('authtoken', res.token);
        // Backbone.history.navigate('', {trigger: true});
      })
      .fail(function(res) {
        utils.validate(self.$el.find('form'), res.responseJSON);
      })
      .always(function () {
        utils.removeLoader(self.$el);
      });
    }

  });

  return RegisterView;

});
