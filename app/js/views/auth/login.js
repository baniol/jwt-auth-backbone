define(function (require) {
  'use strict';

  var Backbone = require('backbone');
  var LoginTemplate = require('text!templates/auth/login.html');
  var utils = require('utils');
  var config = require('config');

  var LoginView = Backbone.View.extend({

    initialize: function () {
      this.render();
    },

    events: {
      "submit .login-form": "login"
    },

    template: _.template(LoginTemplate),

    render: function () {
      this.$el.html(this.template());
      return this;
    },

    login: function (e) {
      e.preventDefault();
      var self = this;

      utils.loader(this.$el, 'Logging in');

      var credentials = {
        username: this.$('.username_email-input').val().trim(),
        password: this.$('.password-input').val().trim()
      };
      $.ajax({
        url: config.domain + '/login',
        data: JSON.stringify(credentials),
        type: 'POST',
        contentType:'application/json'
      })
      .done(function (res) {
        Backbone.trigger('auth:authorized', res);
        Backbone.history.navigate('', {trigger: true});
      })
      .fail(function (res) {
        res = res.responseJSON;
        utils.alert('alert', 'User not authorized!');
      })
      .always(function () {
        utils.removeLoader(self.$el);
      });
    }

  });

  return LoginView;

});
