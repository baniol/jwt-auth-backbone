define(function (require) {
  'use strict';

  var Backbone = require('backbone');
  var ForgotTemplate = require('text!templates/auth/forgot.html');
  var utils = require('utils');
  var config = require('config');

  var ForgotView = Backbone.View.extend({

    initialize: function () {
      this.render();
    },

    events: {
      "submit .forgot-form": "send"
    },

    template: _.template(ForgotTemplate),

    render: function () {
      this.$el.html(this.template());
      return this;
    },

    send: function (e) {
      e.preventDefault();
      var self = this;
      this.$el.find('.columns.error').removeClass('error');
      utils.loader(this.$el, 'Sending');
      var email = this.$('.email-input').val();
      $.ajax({
        url: config.domain + '/remindpassword',
        data: {email: email},
        type: 'POST'
      })
      .done(function (res) {
        utils.alert('success', 'Password sent');
      })
      .fail(function (res, type, msg) {
        res = res.responseJSON;
        utils.validate(self.$el.find('form'), res);
      })
      .always(function () {
        utils.removeLoader(self.$el);
      });
    }

  });

  return ForgotView;

});
