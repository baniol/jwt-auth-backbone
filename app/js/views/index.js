define(function (require) {
  'use strict';

  var Backbone = require('backbone');
  var IntexTemplate = require('text!templates/index.html');

  var IndexView = Backbone.View.extend({

    initialize: function () {
      this.render();
    },

    template: _.template(IntexTemplate),

    render: function () {
      this.$el.html(this.template());
      return this;
    }

  });

  return IndexView;
});