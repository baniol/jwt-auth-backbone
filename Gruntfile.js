module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  var SERVER_PORT = 9000;
  var pkg = grunt.file.readJSON('package.json');
  var appVersion = pkg.version;

  grunt.initConfig({
      config: {
        version: appVersion,
        tmp: 'tmp',
        dist: 'dist',
        src: 'app',
        scss: 'scss'
      },
      watch: {
          options: {
              nospawn: false,
              //livereload: true
          },
          sass: {
              files: ['<%= config.scss %>/_main.scss'],
              tasks: ['sass']
          },
      },
      connect: {
        server: {
            options: {
              port: SERVER_PORT,
              base: '<%= config.src %>'
            }
        }
      },
      clean: {
          dist: ['<%= config.tmp %>', '<%= config.dist %>'],
          server: '<%= config.tmp %>'
      },
      processhtml: {
        dist: {
          options: {
            process: true,
            data: {
              version: appVersion
            },
          },
          files: {
            '<%= config.dist %>/index.html': ['<%= config.tmp %>/index.html']
          }
        }
      },
      sass: {
        dist: {
          options: {
            style: 'compressed',
            sourcemap: 'none'
          },
          files: {
            '<%= config.tmp %>/css/styles_<%= config.version %>.css':'scss/styles.scss'
          }
        }
      },
      copy: {
        dist: {
          files: [
          {
            src: ['<%= config.src %>/bower_components/foundation-icon-fonts/foundation-icons.{eot,svg,ttf,woff}', '<%= config.tmp %>/css/styles_<%= config.version %>.css'],
            dest: '<%= config.dist %>/css/',
            expand: true,
            flatten: true
          },
          {
            src: '<%= config.src %>/bower_components/requirejs/require.js',
            dest: '<%= config.tmp %>/js/',
            expand: true,
            flatten: true
          }
          ]
        }
      },
      requirejs: {
        dist: {
            // Options: https://github.com/jrburke/r.js/blob/master/build/example.build.js
            options: {
                //almond: true,
                //appDir: 'app',
                baseUrl: '<%= config.src %>/js',
                optimize: 'uglify',
                //paths: {
                    //'templates': '../../.tmp/js/templates'
                //},
                preserveLicenseComments: false,
                useStrict: true,
                name: 'main',
                out: '<%= config.tmp %>/js/main.js',
                //dir: 'dist',
                mainConfigFile: '<%= config.src %>/js/main.js'
            }
        }
    },
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: ['<%= config.tmp %>/js/require.js', '<%= config.tmp %>/js/main.js'],
        dest: '<%= config.dist %>/js/app_' + appVersion + '.js',
      },
    },
    replace: {
      dist: {
        options: {
          patterns: [
            {match: /js\/app\.js/, replacement: 'js/app_' + appVersion + '.js'},
            {match: /css\/styles\.css/, replacement: 'css/styles_' + appVersion + '.css'}
          ]
        },
        files: [
          {src: ['<%= config.src %>/index.html'], dest: '<%= config.tmp %>/index.html'}
        ]
      }
    }
  });

  grunt.registerTask('build', [
    'clean:dist',
    'sass:dist',
    'copy:dist',
    'replace:dist',
    'processhtml:dist',
    'requirejs:dist',
    'concat:dist'
  ]);

  grunt.registerTask('serve', function (target) {
      grunt.task.run([
          'copy:dist',
          'sass:dist',
          'connect',
          'watch'
      ]);
  });
};
